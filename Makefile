# ASM_SOURCES = $(wildcard src/*.asm src/drivers.*.asm)
ASM_SOURCES = $(shell find src -type f -name '*.asm')
C_SOURCES = $(shell find src -type f -name '*.c')
OBJ_FILES = $(ASM_SOURCES:.asm=.o) $(C_SOURCES:.c=.o)

CC = i386-elf-gcc
AS = nasm
LD = i386-elf-ld
GDB = i386-elf-gdb

CC_FLAGS = -g -Wall -Werror -Wextra -ffreestanding -m32 -I src/libc/include

BUILD_DIR = build

kernel.elf: $(OBJ_FILES)
	$(LD) -o $(BUILD_DIR)/$@ -T link.ld $^

run: kernel.elf
	qemu-system-i386 -kernel build/kernel.elf -drive file=hdd.img,format=raw,index=0,media=disk 
#-machine type=pc-i440fx-3.1

debug: kernel.elf
	qemu-system-i386 -d int,cpu_reset  -s -S -kernel build/kernel.elf -drive file=hdd.img,format=raw,index=0,media=disk &
	$(GDB) -ex "target remote localhost:1234" -ex "symbol-file build/kernel.elf" 

%.o: %.asm
	$(AS) -g $< -f elf32 -o $@

%.o: %.c
	$(CC) $(CC_FLAGS) -o $@ -c $<

clean:
	rm -f $(OBJ_FILES) $(BUILD_DIR)/*