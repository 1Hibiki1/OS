#include "include/fcntl.h"
#include "../unistd.h"

uint32_t open(char* path, uint32_t mode){
    return syscall(__SYSCALL_OPEN, (uint32_t)path, mode, 0, 0, 0);
}