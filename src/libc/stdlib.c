#include "stdlib.h"
#include "../unistd.h"

void exit(uint32_t code){
    syscall(__SYSCALL_EXIT, code, 1, 2, 3, 4);
    while(1);
}