#ifndef _STDLIB_H_
#define _STDLIB_H_

#include <stdint.h>

void exit(uint32_t code);

#endif