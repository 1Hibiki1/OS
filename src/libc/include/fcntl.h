#ifndef _FCNTL_H_
#define _FCNTL_H_

#include <stdint.h>

uint32_t open(char* path, uint32_t mode);

#endif