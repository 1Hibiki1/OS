#ifndef _STRING_H_
#define _STRING_H_

#include <stdint.h>

uint32_t strlen(char* str);
int8_t strcmp(char* str1, char* str2);
uint8_t streq(char* str1, char* str2);

#endif