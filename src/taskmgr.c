#include "taskmgr.h"
#include "util/list.h"
#include "kmem.h"

list_t task_list;
int32_t cur_task;
uint32_t num_tasks;
uint32_t fg_task;

uint32_t taskmgr_init(){
    task_list = list_new();
    fg_task = -1;
    cur_task = -1;
    num_tasks = 0;

    return 0;
}

int32_t taskmgr_get_cur_task(){
    return cur_task;
}

void taskmgr_set_cur_task(uint32_t n){
    cur_task = n;
}

uint32_t taskmgr_get_num_tasks(){
    return num_tasks;
}

process_t* taskmgr_get_cur_proc(){
    return (process_t*)list_get_idx(task_list, cur_task);
}

void taskmgr_set_fg_task(uint32_t n){
    fg_task = n;
}

int32_t taskmgr_get_fg_task(){
    return fg_task;
}

process_t* taskmgr_get_fg_proc(){
    return (process_t*)list_get_idx(task_list, fg_task);
}

uint32_t taskmgr_add_task(process_t* proc){
    // task_list[num_tasks] = proc;
    list_add(task_list, proc);
    num_tasks++;

    return 0;
}

uint32_t taskmgr_remove_task(process_t* proc){
    list_remove(task_list, proc);
    proc->state = __STATE_TERMINATED;
    
    kfree(proc->stack);
    num_tasks--;
    return 0;
}