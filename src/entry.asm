global _start
global kernel_heap_start
extern kmain

MAGIC_NUMBER equ 0x1BADB002
CHECKSUM equ -MAGIC_NUMBER
STACK_SIZE equ 0x1000000 ; 16 MB
MAX_BLOCK_SIZE equ 0xffffff


section .text

align 4
    dd MAGIC_NUMBER
    dd 0x00
    dd CHECKSUM

_start:
    mov ebp, stack
    mov esp, ebp
    cli
    call kmain
    hlt


section .bss

resb STACK_SIZE
stack:
kernel_heap_start:
resb MAX_BLOCK_SIZE
