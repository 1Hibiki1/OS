#ifndef _PROCESS_H_
#define _PROCESS_H_

#include <stdint.h>
#include "util/list.h"
#include "vfs.h"

typedef enum {
    __STATE_RUNNING,
    __STATE_TERMINATED,
    __STATE_WAITING
} __TASK_STATE;

typedef struct {
    uint32_t edi;
    uint32_t esi;
    uint32_t ebp;
    uint32_t esp0;

    uint32_t ebx;
    uint32_t edx;
    uint32_t ecx;
    uint32_t eax;

    uint32_t esp;

    uint32_t eip;
    uint32_t cs;
    uint32_t eflags;
} __attribute__((packed)) cpu_state_t;

typedef struct {
    cpu_state_t cpu_state;
    __TASK_STATE state;
    list_t file_descriptors;
    uint32_t fcount;
    uint8_t* stack;
} process_t;


uint32_t process_init(process_t* p, uint32_t entry);
uint32_t process_copy_state(process_t* src, process_t* dest);
vfs_file_t process_get_by_fd(process_t* p, uint32_t fd);
uint32_t process_add_open_file(process_t* p, vfs_file_t f);

#endif