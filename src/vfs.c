#include "vfs.h"
#include "kmem.h"

#include "util/list.h"
#include "string.h"

#include <stddef.h>

#define VFS_FILE_SIZE sizeof(vfs_file)

list_t open_file_list;
uint32_t open_file_num;

uint32_t vfs_init(){
    open_file_list = list_new();
    open_file_num = 0;
    return 0;
}

vfs_file_t vfs_new_file(char* fname, uint32_t buf_size){
    vfs_file_t f = (vfs_file_t)kmalloc(VFS_FILE_SIZE);

    f->file_name = fname;
    f->buffer = (uint8_t*)kmalloc(buf_size);
    f->buffer_sz = buf_size;
    f->file_pos = 0;
    f->refcount = 0;

    list_add(open_file_list, f);

    open_file_num++;

    return f;
}

vfs_file_t vfs_open(char* path){
    for(uint32_t i = 0; i < open_file_num; i++){
        vfs_file_t cur = list_get_idx(open_file_list, i);
        if(streq(path, cur->file_name)) return cur;
    }

    return NULL;
}

void vfs_delete(vfs_file_t f){
    kfree(f->buffer);
    list_remove(open_file_list, f);
    kfree(f);
}

uint32_t vfs_write_buffer(vfs_file_t f, uint8_t* data, uint32_t len){
    uint8_t* buf = f->buffer;
    uint32_t i;

    uint32_t max_len = f->buffer_sz;

    for(i = 0; i < len || i < max_len; i++){
        buf[i] = data[i];
    }

    if(i == (max_len - 1)) {
        // TODO: handle scroll
    }

    return i;
}