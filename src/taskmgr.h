#ifndef _TASKMGR_H_
#define _TASKMGR_H_

#include "process.h"

uint32_t taskmgr_init();

int32_t taskmgr_get_cur_task();
void taskmgr_set_cur_task(uint32_t n);
process_t* taskmgr_get_cur_proc();

uint32_t taskmgr_get_num_tasks();

void taskmgr_set_fg_task(uint32_t n);
int32_t taskmgr_get_fg_task();
process_t* taskmgr_get_fg_proc();

uint32_t taskmgr_add_task(process_t* proc);
uint32_t taskmgr_remove_task(process_t* proc);

#endif