global syscall_entry
extern syscall_run

syscall_entry:
    push ebp
    push edi
    push esi
    push edx
    push ecx
    push ebx
    
    push esp
    call syscall_run
    add esp, 28

    iretd

