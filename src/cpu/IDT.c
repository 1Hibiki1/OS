#include "IDT.h"
#include "PIC.h"
#include "../drivers/screen.h"
#include "../drivers/port.h"

#define IDT_SIZE 256

idt_entry idt[256];
idt_entry_encoded idt_encoded[256];
idt_rec irec;

uint32_t interrupt_handlers[256];
extern void* interrupt_handlers_entry[];

void encode_idt_entry(idt_entry*, idt_entry_encoded*);
void dummy_handler();
extern void load_idt(uint32_t);


uint32_t IDT_init(){
    for(uint16_t i = 0; i < IDT_SIZE; i++){
        interrupt_handlers[i] = (uint32_t)dummy_handler;
    }

    for(uint16_t i = 0; i < IDT_SIZE; i++){
        idt[i].address = (uint32_t)interrupt_handlers_entry[i];
        idt[i].DPL = 0x00;  // ring 0 - kernel mode
    }


    for(uint16_t i = 0; i < IDT_SIZE; i++){ 
        encode_idt_entry(&idt[i], &idt_encoded[i]);
    }

    irec.size = 256*8 - 1;
    irec.adress = (uint32_t)(&idt_encoded[0]);

    load_idt((uint32_t)(&irec));
    return 0;
}

void encode_idt_entry(idt_entry* src, idt_entry_encoded* target){
    target->offset_1 = (src->address & 0xffff);
    target->selector = 0x08;
    target->zero = 0;
    target->type_attributes = 0x8e;
    target->offset_2 = ((src->address & 0xffff0000)>>16);
}

uint32_t IDT_install_handler(uint8_t int_no, uint32_t handler){
    interrupt_handlers[int_no] = handler;
    return 0;
}

uint32_t IDT_install_entry_point(uint8_t int_no, uint32_t entry){
    idt[int_no].address = entry;
    encode_idt_entry(&idt[int_no], &idt_encoded[int_no]);
    load_idt((uint32_t)&irec);
    return 0;
}


void dummy_handler(){
    PIC_sendEOI(9);
}