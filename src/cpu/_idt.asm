[bits 32]

global load_idt
global interrupt_handlers_entry

extern dummy_handler
extern interrupt_handlers
extern kmemcpy

load_idt:
    mov eax, [esp + 4]
    lidt [eax]
	sti
    ret



%macro handler_template 1
	handler%1:
		pushad
		cld

		push esp
		call [interrupt_handlers + 4*%1]
		add esp, 4

		popad
		iretd

%endmacro

%macro handler_template_name_export 1
global handler%1
%endmacro
%assign i 0
%rep 256

handler_template_name_export i 
handler_template i


%assign i i+1
%endrep


%macro handler_template_name_dd 1
dd handler%1
%endmacro
interrupt_handlers_entry:
%assign i 0
%rep 256
handler_template_name_dd i
%assign i i+1
%endrep
