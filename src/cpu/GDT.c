#include "GDT.h"

#define GDT_SIZE 2

// commmon values
#define COMMON_FLAGS 0b1100
#define COMMON_BASE 0
#define COMMON_LIMIT 0xfffff

// access bytes
// #define CODE_SEG_AB 0b01111001
// #define DATA_SEG_AB 0b01001001

#define CODE_SEG_AB 0b10011010
#define DATA_SEG_AB 0b10010010

typedef struct {
    uint32_t limit;
    uint32_t base;
    uint8_t access_byte;
    uint8_t flags;
} gdt_entry;

typedef struct {
    uint16_t size;
    uint32_t address;
} __attribute__((packed)) gdtr;

typedef gdt_entry* gdt_entry_t;

gdt_entry gdt[2];
uint8_t gdt_encoded[3][8];
gdtr GDT_r;


void encodeGdtEntry(uint8_t *target, gdt_entry_t source);
// extern void setGdt(uint16_t sz, uint32_t adr);
extern void load_gdt(gdtr*);
extern void reload_cs();

uint32_t GDT_init(){

    // set everything to 0
    for (uint8_t i = 0; i < 3; i++) {
        for (uint8_t j = 0; j < 8; j++){
            gdt_encoded[i][j] = 0;
        }
    }

    for (uint8_t i = 0; i < GDT_SIZE; i++) {
        gdt[i].base = COMMON_BASE;
        gdt[i].limit = COMMON_LIMIT;
        gdt[i].access_byte = 0;
        gdt[i].flags = COMMON_FLAGS;
    }

    // first entry - code seg
    gdt[0].access_byte = CODE_SEG_AB;

    // second entry - data seg
    gdt[1].access_byte = DATA_SEG_AB;


    for (uint8_t i = 1; i < GDT_SIZE + 1; i++){
        encodeGdtEntry(gdt_encoded[i], &gdt[i-1]);
    }

    // TODO: should it be size - 1?
    GDT_r.size = 24;
    GDT_r.address = (uint32_t)(&gdt_encoded[0]);

    load_gdt(&GDT_r);

    // this function doesnt work HELP (details in asm file)

    // IT WORKS NOW! access byte and flags were completely messed up
    // so it didnt work b4
    reload_cs();

    // this didnt work :0

    // also probably bcuz gdt content was messed up
    // setGdt(24, (uint32_t)(&gdt_encoded[0]));
    
    return 0;
}

// from https://wiki.osdev.org/GDT_Tutorial
void encodeGdtEntry(uint8_t *target, gdt_entry_t source)
{
    // Check the limit to make sure that it can be encoded
    // if (source.limit > 0xFFFFF) {("GDT cannot encode limits larger than 0xFFFFF");}
 
    // Encode the limit
    target[0] = source->limit & 0xFF;
    target[1] = (source->limit >> 8) & 0xFF;
    target[6] = (source->limit >> 16) & 0x0F;
 
    // Encode the base
    target[2] = source->base & 0xFF;
    target[3] = (source->base >> 8) & 0xFF;
    target[4] = (source->base >> 16) & 0xFF;
    target[7] = (source->base >> 24) & 0xFF;
 
    // Encode the access byte
    target[5] = source->access_byte;
 
    // Encode the flags
    target[6] |= (source->flags << 4);
}