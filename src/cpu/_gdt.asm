global load_gdt
global reload_cs


load_gdt:
    cli
    xor ax, ax
    mov ds, ax
    mov eax, [esp + 4]
    lgdt [eax]
    ret

reload_cs:
    mov   ax, 0x10

    ; this causes ds to be set to 0 fsr and eip to 0xe05b
    ; that was bcuz gdt access bytes nd flags were messed up
    mov   ds, ax
    mov   es, ax
    mov   fs, ax
    mov   gs, ax
    mov   ss, ax

    jmp   0x08:rCS

    rCS:
    ret
    

; these didnt work
; from https://wiki.osdev.org/GDT_Tutorial

; global setGdt
; gdtr DW 0 ; For limit storage
;      DD 0 ; For base storage
 
; setGdt:
;     pusha
;     mov   dx, word [esp + 4]
;     mov   [gdtr], dx
;     mov   eax, [esp + 8]
;     mov   [gdtr + 2], eax
;     lgdt  [gdtr]
;     popa
;     ret