#ifndef _PIC_H_
#define _PIC_H_

#include <stdint.h>

uint32_t PIC_init();
uint32_t PIC_sendEOI(uint8_t irq);


#endif