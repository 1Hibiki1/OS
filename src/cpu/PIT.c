#include "PIT.h"
#include "IDT.h"
#include "../drivers/port.h"

// based on https://github.com/stevej/osdev/blob/master/kernel/devices/timer.c

#define PIT_A 0x40
#define PIT_B 0x41
#define PIT_C 0x42
#define PIT_CONTROL 0x43

#define PIT_MASK 0xFF
#define PIT_SCALE 1193180
#define PIT_SET 0x36


uint32_t PIT_freq = 100;

uint32_t PIT_time_phase(uint32_t freq){
    uint32_t divisor = PIT_SCALE / freq;
    outb(PIT_CONTROL, PIT_SET);
	outb(PIT_A, divisor & PIT_MASK);
	outb(PIT_A, (divisor >> 8) & PIT_MASK);
    return 0;
}

uint32_t PIT_init(uint32_t freq){
    PIT_freq = freq;
    PIT_time_phase(PIT_freq);
    return 0;
}




uint32_t PIT_install_handler(uint32_t handler){
    IDT_install_handler(TIMER_IRQ, handler);
    return 0;
}
