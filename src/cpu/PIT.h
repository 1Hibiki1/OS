#ifndef _PIT_H_
#define _PIT_H_

#include <stdint.h>

#define TIMER_IRQ 0x20

uint32_t PIT_init(uint32_t freq);
uint32_t PIT_install_handler(uint32_t handler);

#endif