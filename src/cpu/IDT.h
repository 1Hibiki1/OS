#ifndef _IDT_H_
#define _IDT_H_

#include <stdint.h>

typedef struct {
    uint32_t address;
    uint8_t DPL;
} idt_entry;


typedef struct {
    // uint32_t high_32;
    // uint32_t low_32;
    uint16_t offset_1;        // offset bits 0..15
    uint16_t selector;        // a code segment selector in GDT or LDT
    uint8_t  zero;            // unused, set to 0
    uint8_t  type_attributes; // gate type, dpl, and p fields
    uint16_t offset_2;
} __attribute__((packed)) idt_entry_encoded;


typedef struct {
    uint16_t size;
    uint32_t adress;
} __attribute__((packed)) idt_rec;


uint32_t IDT_init();
uint32_t IDT_install_handler(uint8_t int_no, uint32_t handler);
uint32_t IDT_install_entry_point(uint8_t int_no, uint32_t entry);

#endif