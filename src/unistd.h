#ifndef _UNISTD_H_
#define _UNISTD_H_

#include <stdint.h>
#include "syscall.h"

extern uint32_t syscall(__SYSCALL s, uint32_t arg1, uint32_t arg2, uint32_t arg3, uint32_t arg4, uint32_t arg5);

#endif