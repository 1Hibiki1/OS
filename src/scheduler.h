#ifndef _SCHEDULER_H_
#define _SCHEDULER_H_

#include <stdint.h>

uint32_t scheduler_init();
uint32_t scheduler_run();

#endif