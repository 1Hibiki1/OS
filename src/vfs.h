#ifndef _VFS_H_
#define _VFS_H_

#include <stdint.h>

typedef struct {
    char* file_name;
    uint32_t refcount;
    uint32_t file_pos;
    uint8_t* buffer;
    uint32_t buffer_sz;
} vfs_file;

typedef vfs_file* vfs_file_t;

uint32_t vfs_init();
vfs_file_t vfs_new_file(char* fname, uint32_t buf_size);
void vfs_delete(vfs_file_t f);
uint32_t vfs_write_buffer(vfs_file_t f, uint8_t* data, uint32_t len);
vfs_file_t vfs_open(char* path);

#endif