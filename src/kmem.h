#ifndef _KMEM_H_
#define _KMEM_H_

#include <stdint.h>

uint32_t kmemcpy(void* src, void* dest, uint32_t num_bytes);
uint32_t kmem_heap_init();
void* kmalloc(uint32_t size);
void kfree(void* ptr);

#endif