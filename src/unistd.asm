global syscall

syscall:
    ; pushad
    push ebx
    push ecx
    push edx
    push esi
    push edi
    push ebp
    
    mov ebx, [esp+28]
    mov ecx, [esp+32]
    mov edx, [esp+36]
    mov esi, [esp+40]
    mov edi, [esp+44]
    mov ebp, [esp+48]

    int 0x69
    
    pop ebp
    pop edi
    pop esi
    pop edx
    pop ecx
    pop ebx
    ; popad
    ret