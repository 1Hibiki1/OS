#define ELFNOTE(name, type, desc)                \
     asm (".pushsection .note, \"a\", @note;"     \
     ".align 4;"                                  \
     ".long 2f - 1f;"       /* namesz */          \
     ".long 4f - 3f;"       /* descsz */          \
     ".long " type  ";" /* type   */          \
     "1: .asciz \"" #name "\";" /* name   */      \
     "2:.align 4;"                                \
     "3: " desc ";"         /* desc   */          \
     "4:.align 4;"                                \
     ".popsection;")

#define XEN_ELFNOTE_PHYS32_ENTRY "18"
ELFNOTE(Xen, XEN_ELFNOTE_PHYS32_ENTRY, ".long _start");



#include "drivers/screen.h"
#include "drivers/keyboard.h"
#include "drivers/ata.h"

#include "cpu/GDT.h"
#include "cpu/IDT.h"
#include "cpu/PIC.h"
#include "cpu/PIT.h"

#include "scheduler.h"
#include "taskmgr.h"
#include "process.h"
#include "kmem.h"
#include "syscall.h"
#include "vfs.h"

#include "stdlib.h"
#include "fcntl.h"



void task1(){
    open("/dev/stdin", 0);
    open("/dev/stdout", 0);

    for(uint8_t i = 0; i < 10; i++)
        kprint(" t1 ");
}

void task2(){
    open("/dev/stdin", 0);
    open("/dev/stdout", 0);

    for(uint8_t i = 0; i < 10; i++)
        kprint(" t2 ");
}

void task3(){
    open("/dev/stdin", 0);
    open("/dev/stdout", 0);

    for(uint8_t i = 0; i < 10; i++)
        kprint(" t3 ");

    // while(1);
}


void kmain(){
    GDT_init();
    IDT_init();
    PIC_init();

    kmem_heap_init();

    vfs_init();
    syscall_init();

    screen_init();
    keyboard_init();

    // PIT_install_handler((uint32_t)time_handler);
    // PIT_init(10);

    process_t t1;
    process_init(&t1, (uint32_t)task1);

    process_t t2;
    process_init(&t2, (uint32_t)task2);

    process_t t3;
    process_init(&t3, (uint32_t)task3);

    taskmgr_init();
    taskmgr_add_task(&t1);
    taskmgr_add_task(&t3);
    taskmgr_add_task(&t2);

    scheduler_init();
    scheduler_run();

    // disk test code
    // kprint("master ata: ");
    // ata_init(DEVICE_MASTER);
    // kprint("     slave ata: ");
    // ata_init(DEVICE_SLAVE);

    // uint16_t data[256];
    // data[0] = 0x4748;
    // data[1] = 0x4950;
    // data[2] = 0;

    // kprint("  current data: ");
    // kprint((char*)data);

    // ata_write(DEVICE_MASTER, 0, 1, data);

    // data[0] = 0;
    // data[1] = 0;
    // data[2] = 0;

    // ata_read(DEVICE_MASTER, 0, 1, data);

    // kprint("  new data: ");
    // kprint((char*)data);



    // for(uint8_t i = 0; i < 10; i++)
    // kprint("Hello Happy World!!");

    // while (1) {}
    for(;;) asm("hlt");
}