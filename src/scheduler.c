#include "scheduler.h"
#include "taskmgr.h"

#include "drivers/screen.h"

#include "cpu/PIT.h"
#include "cpu/PIC.h"
#include "cpu/IDT.h"

extern void scheduler_callback_entry();

uint32_t scheluder_freq = 10;

uint32_t scheduler_callback(process_t* cpu_state){
    // interrupts are disabled here so we will not get another one
    // until end of handler, so send EOI now
    PIC_sendEOI(0);


    uint8_t* cur_esp = (uint8_t*)cpu_state->cpu_state.esp;
    cur_esp += 12;
    cpu_state->cpu_state.esp = (uint32_t)cur_esp;

    process_t* cur_proc = taskmgr_get_cur_proc();

    int16_t cur_task = taskmgr_get_cur_task();

    if(cur_proc->state == __STATE_TERMINATED)
        taskmgr_remove_task(cur_proc);

    uint8_t max_tasks = taskmgr_get_num_tasks();
    if(max_tasks == 0) return 0;

    else if(cur_task > (int16_t)-1) {
        // save state
        process_t* cur_proc = taskmgr_get_cur_proc();
        process_copy_state(cpu_state, cur_proc);
    }

    cur_task = taskmgr_get_cur_task();
    uint8_t next_task = cur_task+1;

    if(next_task >= max_tasks) next_task = 0;

    taskmgr_set_cur_task(next_task);
    process_t* next_proc = taskmgr_get_cur_proc();

    // kprint_char((char)(next_task+48));
    // kprint(" ");

    return (uint32_t)(&(next_proc->cpu_state));
}

uint32_t scheduler_init(){
    PIT_init(scheluder_freq);
    return 0;
}


uint32_t scheduler_run() {
    IDT_install_entry_point(TIMER_IRQ, (uint32_t)scheduler_callback_entry);
    return 0;
}