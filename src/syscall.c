#include "syscall.h"
#include "cpu/IDT.h"

#include "taskmgr.h"
#include "process.h"
#include "drivers/screen.h"

#include "vfs.h"

extern void syscall_entry();

uint32_t syscall_init(){
    IDT_install_entry_point(SYSCALL_INT, (uint32_t)syscall_entry);
    return 0;
}

uint32_t syscall_run(pt_regs* args){
    __SYSCALL type = (__SYSCALL)(args->ebx);
    process_t* cur = taskmgr_get_cur_proc();

    switch(type){
        case __SYSCALL_EXIT: {
            cur->state = __STATE_TERMINATED;
            kprint("Process terminated. ");
            break;
        }

        case __SYSCALL_OPEN: {
            char* path = (char*)args->ecx;
            // uint32_t mode = args->edx;
            
            vfs_file_t f = vfs_open(path);
            uint32_t fd = process_add_open_file(cur, f);

            return fd;

            break;
        }

        case __SYSCALL_READ: {
            break;
        }

        case __SYSCALL_WRITE: {
            uint32_t fd = args->ecx;
            uint8_t* buf = (uint8_t*)args->edx;
            uint32_t nbytes = args->esi;

            vfs_write_buffer(process_get_by_fd(cur, fd), buf, nbytes);
            break;
        }

        case __SYSCALL_CLOSE: {
            break;
        }

        case __SYSCALL_FORK: {
            break;
        }

        case __SYSCALL_KILL: {
            break;
        }

        default: {
            break;
        }
    }
    return 0;
}