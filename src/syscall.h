#ifndef _SYSCALL_H_
#define _SYSCALL_H_

#include <stdint.h>

#define SYSCALL_INT 0x69

typedef enum {
    __SYSCALL_EXIT,
    __SYSCALL_OPEN,
    __SYSCALL_READ,
    __SYSCALL_WRITE,
    __SYSCALL_CLOSE,
    __SYSCALL_FORK,
    __SYSCALL_KILL
} __SYSCALL;

typedef struct {
    uint32_t ebx, ecx, edx, esi, edi, ebp;
} pt_regs;

uint32_t syscall_init();
uint32_t syscall_run(pt_regs*);

#endif