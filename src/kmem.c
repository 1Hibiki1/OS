#include "kmem.h"
#include <stddef.h>

#define MAX_BLOCK_SIZE 0xffffff  // ~168 MB
#define MIN_BLOCK_SIZE 32
extern uint32_t kernel_heap_start;

typedef struct blk_t{
    uint32_t allocated;
    uint32_t size;
    struct blk_t* prev_block;
    struct blk_t* next_block;
} block_t;

block_t* find_block(uint32_t);
void split_block(block_t*, uint32_t);

block_t* block_start = NULL;

uint32_t kmem_heap_init(){
    block_start = (block_t*)&kernel_heap_start;
    block_start->allocated = 0;
    block_start->size = MAX_BLOCK_SIZE;
    block_start->prev_block = NULL;
    block_start->next_block = NULL;
    return 0;
}

void* kmalloc(uint32_t size){
    if(block_start == NULL) return NULL;
    
    uint32_t aligned_size = (size%4 == 0)? size : ((size & 0xfffffffc) + 4);

    block_t* block = find_block(aligned_size);
    split_block(block, aligned_size);

    block->allocated = 1;

    return (void*)((uint8_t*)block + sizeof(block_t));
}

void kfree(void* ptr){
    block_t* block = (block_t*)ptr;
    block->allocated = 0;
}

uint32_t kmemcpy(void* srcv, void* destv, uint32_t n){
    uint8_t* src = (uint8_t*)srcv;
    uint8_t* dest = (uint8_t*)destv;

    for(uint32_t i = 0; i < n; i++){
        dest[i] = src[i];
    }

    return 0;
}

block_t* find_block(uint32_t sz){
    for(block_t* b = block_start; b != NULL; b = b->next_block){
        if((!b->allocated) && (b->size >= sz)) return b;
    }

    return NULL;
}

void split_block(block_t* b, uint32_t sz){
    if((b->size - sz) >= MIN_BLOCK_SIZE){
        uint32_t next_size = (b->size - sz);
        block_t* next_block = (block_t*)(((uint8_t*)b) + sz + sizeof(block_t));
        
        next_block->size = next_size;
        next_block->prev_block = b;
        next_block->allocated = 0;
        next_block->next_block = NULL;
    
        b->size = sz;
        b->next_block = next_block;
    }
}