#ifndef _LIST_H_
#define _LIST_H_

#include <stdint.h>

typedef struct lst {
    struct lst* prev;
    struct lst* next;
    void* payload;
} list;

typedef list* list_t;

list_t list_new();
void list_free(list_t l);

void* list_get_idx(list_t l, uint32_t idx);

void list_add(list_t l, void* item);
void list_add_idx(list_t l, uint32_t idx, void* item);

void list_remove(list_t l, void* item);
void list_remove_idx(list_t l, uint32_t idx);

list_t get_last(list_t l);



#endif