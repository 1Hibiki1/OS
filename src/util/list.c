#include "list.h"
#include "../kmem.h"

#include <stddef.h>

#define LIST_NODE_SIZE sizeof(list)

list_t list_new(){
    list_t l = (list_t)kmalloc(LIST_NODE_SIZE);
    l->next = NULL;
    l->prev = NULL;
    l->payload = NULL;

    return l;
}

void list_free(list_t l){
    for(list_t i = l; i != NULL; i = i->next){
        kfree(i);
    }
}

list_t get_last(list_t l){
    while(l->next != NULL) l = l->next;
    return l;
}

void* list_get_idx(list_t l, uint32_t idx){
    for(int32_t i = -1; i < (int32_t)idx; i++){
        l = l->next;
    }

    return l->payload;
}

void list_add(list_t l, void* item){
    list_t new_l = (list_t)kmalloc(LIST_NODE_SIZE);
    list_t last = get_last(l);

    new_l->payload = item;
    new_l->prev = last;
    new_l->next = NULL;

    last->next = new_l;
}

void list_add_idx(list_t l, uint32_t idx, void* item){
    // BROKEN
    for(int32_t i = -1; i < (int32_t)idx; i++){
        l = l->next;
    }

    list_t new_item = (list_t)kmalloc(LIST_NODE_SIZE);
    new_item->payload = item;

    list_t next_next = l->next;
    l->next = new_item;
    new_item->next = next_next;

    next_next->prev = new_item;
    new_item->prev = l;
}


void list_remove(list_t l, void* item){
    for(l = l->next;l != NULL && l->payload != item; l=l->next);
    if(l == NULL) return;

    if(l->prev != NULL)
        l->prev->next = l->next;

    if(l->next != NULL)
        l->next->prev = l->prev;

    kfree(l);
    l = NULL;
}

void list_remove_idx(list_t l, uint32_t idx);