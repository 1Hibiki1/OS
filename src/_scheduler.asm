
extern kmemcpy
extern scheduler_callback
global scheduler_callback_entry

scheduler_callback_entry:
    push esp
	pushad
	cld

	push esp
	call scheduler_callback

    test eax, eax
    jnz .continue

    ; eax is 0
    add esp, 4
    popad
    add esp, 4
    iretd

    .continue:
	pop ecx  ; old value of esp

	push 48  ; no. of bytes to copy
	push ecx ; dest
	push eax ; src
	call kmemcpy
	add esp, 12

    ; new process's esp is at [esp + 32]

    mov ebx, esp
    add ebx, 32
    mov eax, [ebx]
    sub eax, 48  ; make space on process stack

    push 48
    push eax ; dest
    mov ebx, esp
    add ebx, 8
    push ebx ; src
    call kmemcpy
    add esp, 12
	
    popad
    pop esp
    sub esp, 12

	iretd