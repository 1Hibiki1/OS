#include "screen.h"
#include "port.h"
#include "../vfs.h"

uint16_t xy_to_pos(uint8_t x, uint8_t y);
void pos_to_xy(uint16_t pos, uint8_t* x, uint8_t* y);
void enable_cursor(uint8_t cursor_start, uint8_t cursor_end);
void update_cursor(int x, int y);

uint8_t* vid_mem = (uint8_t*)VID_MEM;

uint8_t cur_pos_x = 0;
uint8_t cur_pos_y = 0;

uint8_t screen_init(void) {
    enable_cursor(0, 0);
    update_cursor(0, 1);
    kclrscr();
    vfs_new_file("/dev/stdout", VGA_HEIGHT*VGA_WIDTH);
    return 0;
}

uint8_t kclrscr(){
    for(uint16_t i = 0; i < VGA_HEIGHT * VGA_WIDTH; i++){
        vid_mem[i*2] = ' ';
        vid_mem[i*2+1] = 0x0f;
    }
    return 0;
}

uint32_t fb_write(char* buf, uint32_t len) {
    for(uint8_t i = 0; i < len; i++)
        vid_mem[i*2] = buf[i];

    return 0;
}

uint32_t kprint(char* str) {
    uint16_t cur_pos = xy_to_pos(cur_pos_x, cur_pos_y);
    uint16_t i;

    for(i = 0; str[i] != '\0'; i++, cur_pos++){
        vid_mem[cur_pos*2] = str[i];
    }
    
    pos_to_xy(cur_pos, &cur_pos_x, &cur_pos_y);
    update_cursor(cur_pos_x, cur_pos_y+1);

    return i;
}

uint32_t kprint_char(char c){
    uint16_t cur_pos = xy_to_pos(cur_pos_x, cur_pos_y);
    
    vid_mem[cur_pos*2] = c;

    pos_to_xy(cur_pos+1, &cur_pos_x, &cur_pos_y);
    update_cursor(cur_pos_x, cur_pos_y+1);

    return 0;
}


/* Utility functions */

uint16_t xy_to_pos(uint8_t x, uint8_t y) {
    return (y*VGA_WIDTH + x);
}

void pos_to_xy(uint16_t pos, uint8_t* x, uint8_t* y) {
    *y = (uint8_t)(pos / VGA_WIDTH);
    *x = (uint8_t)(pos % VGA_WIDTH);
}


/* cursor functions (from https://wiki.osdev.org/Text_Mode_Cursor) */

void enable_cursor(uint8_t cursor_start, uint8_t cursor_end)
{
	outb(0x3D4, 0x0A);
	outb(0x3D5, (inb(0x3D5) & 0xC0) | cursor_start);
 
	outb(0x3D4, 0x0B);
	outb(0x3D5, (inb(0x3D5) & 0xE0) | cursor_end);
}

void update_cursor(int x, int y)
{
	uint16_t pos = y * VGA_WIDTH + x;

    if (pos > VGA_HEIGHT*VGA_WIDTH){
        pos = 0;
        cur_pos_x = 0;
        cur_pos_y = 0;
    }
 
	outb(0x3D4, 0x0F);
	outb(0x3D5, (uint8_t) (pos & 0xFF));
	outb(0x3D4, 0x0E);
	outb(0x3D5, (uint8_t) ((pos >> 8) & 0xFF));
}