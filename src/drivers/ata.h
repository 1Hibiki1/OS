#ifndef _ATA_H_
#define _ATA_H_

#include <stdint.h>

#define PORT_MASTER 0xa0
#define PORT_SLAVE 0xb0
#define DEVICE_MASTER PORT_MASTER
#define DEVICE_SLAVE PORT_SLAVE

uint32_t ata_init(uint8_t device);
uint32_t ata_read(uint8_t device, uint32_t LBA, uint8_t sect_cnt, uint16_t* buffer);
uint32_t ata_write(uint8_t device, uint32_t LBA, uint8_t sect_cnt, uint16_t* data);

#endif