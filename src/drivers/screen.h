#ifndef _SCREEN_H_
#define _SCREEN_H_

#include <stdint.h>

#define VID_MEM 0xb8000
#define VGA_WIDTH 80
#define VGA_HEIGHT 25

uint8_t screen_init();
uint32_t fb_write(char* buf, uint32_t len);
uint32_t kprint(char* str);
uint32_t kprint_char(char c);
uint8_t kclrscr();

#endif