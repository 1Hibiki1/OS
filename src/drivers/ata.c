#include "ata.h"
#include "port.h"
#include "screen.h"

#define PORT_PRIMARY_BUS 0x1f6

#define PORT_SECTOR_COUNT 0x1f2
#define PORT_LBA_LO 0x1f3
#define PORT_LBA_MID 0x1f4
#define PORT_LBA_HI 0x1f5

#define PORT_COMMAND_IO 0x1f7
#define PORT_DATA 0x1f0

#define CMD_IDENTIFY 0xec
#define CMD_READ 0x20
#define CMD_WRITE 0x30

// private function prototypes
void ata_identify(uint8_t device);


uint32_t ata_init(uint8_t device){
    ata_identify(device);
    kprint("ata initialization done! ");
    return 0;
}


void ata_identify(uint8_t device){
    outb(PORT_PRIMARY_BUS, device);  // select device

    outb(PORT_SECTOR_COUNT, 0);
    outb(PORT_LBA_LO, 0);
    outb(PORT_LBA_MID, 0);
    outb(PORT_LBA_HI, 0);

    outb(PORT_COMMAND_IO, CMD_IDENTIFY);

    uint8_t cmd_data = inb(PORT_COMMAND_IO);

    if(cmd_data == 0){
        kprint("ERROR: drive does not exist ");
        return;
    }

    while ((cmd_data & 0x80) == 0x80) {
        if(cmd_data & 1){
            kprint("ERROR! ");
            return;
        }

        if((inb(PORT_LBA_MID) | inb(PORT_LBA_HI)) != 0){
            kprint("Drive not ATA! ");
            return;
        }

        cmd_data = (inb(PORT_COMMAND_IO));
    }

    uint16_t data[256];

    for(uint16_t i = 0; i < 256; i++){
        data[i] = inw(PORT_DATA);
    }

    kprint("| received data: ");
    kprint((char*)data);
    kprint("end received data | ");
}

uint32_t ata_read(uint8_t device, uint32_t LBA, uint8_t sect_cnt, uint16_t* buffer){
    uint16_t primary_port_data = (device == DEVICE_MASTER) ? 0xe0 : 0xf0;
    primary_port_data |= ((LBA >> 24) & 0x0F);

    outb(PORT_PRIMARY_BUS, primary_port_data);

    outb(0x1f1, 0x0);  // osdev wiki told me to do this lol

    outb(PORT_SECTOR_COUNT, sect_cnt);
    outb(PORT_LBA_LO, (uint8_t)LBA);
    outb(PORT_LBA_MID, (uint8_t)(LBA >> 8));
    outb(PORT_LBA_HI, (uint8_t)(LBA >> 16));

    outb(PORT_COMMAND_IO, CMD_READ);

    uint8_t cmd_data = inb(PORT_COMMAND_IO);

    for(uint8_t i = 0; i < sect_cnt; i++){
        while ((cmd_data & 0x80) == 0x80) {
            if(cmd_data & 1){
                kprint(" READ ERROR! ");
                return 1;
            }

            cmd_data = (inb(PORT_COMMAND_IO));
        }

        for(uint16_t j = 0; j < 256; j++){
            buffer[i*256 + j] = inw(PORT_DATA);
        }
    }
    

    return 0;
}

uint32_t ata_write(uint8_t device, uint32_t LBA, uint8_t sect_cnt, uint16_t* data){
    uint16_t primary_port_data = (device == DEVICE_MASTER) ? 0xe0 : 0xf0;
    primary_port_data |= ((LBA >> 24) & 0x0F);

    outb(PORT_PRIMARY_BUS, primary_port_data);

    outb(0x1f1, 0x0);  // osdev wiki told me to do this lol

    outb(PORT_SECTOR_COUNT, sect_cnt);
    outb(PORT_LBA_LO, (uint8_t)LBA);
    outb(PORT_LBA_MID, (uint8_t)(LBA >> 8));
    outb(PORT_LBA_HI, (uint8_t)(LBA >> 16));

    outb(PORT_COMMAND_IO, CMD_WRITE);

    uint8_t cmd_data = inb(PORT_COMMAND_IO);

    for(uint8_t i = 0; i < sect_cnt; i++){
        while ((cmd_data & 0x80) == 0x80) {
            if(cmd_data & 1){
                kprint(" READ ERROR! ");
                return 1;
            }

            cmd_data = (inb(PORT_COMMAND_IO));
        }

        for(uint16_t j = 0; j < 256; j++){
            outw(PORT_DATA, data[i*256 + j]);
        }

        outb(0x1f1, 0x0);  // osdev wiki told me to do this lol
        outb(PORT_COMMAND_IO, 0xe7);  // flush cache??
    }
    

    return 0;
}