#include "keyboard.h"
#include "screen.h"
#include "port.h"
#include "../cpu/IDT.h"
#include "../cpu/PIC.h"
#include "../vfs.h"

// from https://github.com/szhou42/osdev/blob/master/src/kernel/drivers/keyboard.c
char kbdus[128] = {
    0,  27, '1', '2', '3', '4', '5', '6', '7', '8', /* 9 */
    '9', '0', '-', '=', '\b',   /* Backspace */
    '\t',           /* Tab */
    'q', 'w', 'e', 'r', /* 19 */
    't', 'y', 'u', 'i', 'o', 'p', '[', ']', '\n',       /* Enter key */
    0,          /* 29   - Control */
    'a', 's', 'd', 'f', 'g', 'h', 'j', 'k', 'l', ';',   /* 39 */
    '\'', '`',   0,     /* Left shift */
    '\\', 'z', 'x', 'c', 'v', 'b', 'n',         /* 49 */
    'm', ',', '.', '/',   0,                    /* Right shift */
    '*',
    0,  /* Alt */
    ' ',    /* Space bar */
    0,  /* Caps lock */
    0,  /* 59 - F1 key ... > */
    0,   0,   0,   0,   0,   0,   0,   0,
    0,  /* < ... F10 */
    0,  /* 69 - Num lock*/
    0,  /* Scroll Lock */
    0,  /* Home key */
    0,  /* Up Arrow */
    0,  /* Page Up */
    '-',
    0,  /* Left Arrow */
    0,
    0,  /* Right Arrow */
    '+',
    0,  /* 79 - End key*/
    0,  /* Down Arrow */
    0,  /* Page Down */
    0,  /* Insert Key */
    0,  /* Delete Key */
    0,   0,   0,
    0,  /* F11 Key */
    0,  /* F12 Key */
    0,  /* All other keys are undefined */
};

void kbd_handler(){
    uint8_t scan_code = inb(0x60);

    if(!(scan_code & 0x80))
        kprint_char(kbdus[scan_code]);

    PIC_sendEOI(9);
}

uint32_t keyboard_init(){
    // TODO: do we really need this?? / try n understand this
    uint8_t curmask = inb(0x21);
    outb(0x21, 0xfd & curmask);

    IDT_install_handler(0x21, (uint32_t)kbd_handler);

    vfs_new_file("/dev/stdin", VGA_HEIGHT*VGA_WIDTH);

    return 0;
}