#include "process.h"
#include "kmem.h"
#include "stdlib.h"
#include "vfs.h"

uint32_t process_init(process_t* p, uint32_t entry){
    p->cpu_state.eax = 0;
    p->cpu_state.ebx = 0;
    p->cpu_state.ecx = 0;
    p->cpu_state.edx = 0;
    p->cpu_state.esi = 0;
    p->cpu_state.edi = 0;
    p->cpu_state.ebp = 0;

    p->cpu_state.eip = entry;

    p->cpu_state.cs = 0x08;
    p->cpu_state.eflags = 0x202;

    p->stack = kmalloc(4096);
    p->cpu_state.esp = (uint32_t)(&(p->stack[4092]));

    p->state = __STATE_RUNNING;

    p->file_descriptors = list_new();
    p->fcount = -1;

    ((uint32_t*)p->stack)[1023] = (uint32_t)exit;
    return 0;
}

uint32_t process_copy_state(process_t* src, process_t* dest){
    dest->cpu_state.edi = src->cpu_state.edi;
    dest->cpu_state.esi = src->cpu_state.esi;
    dest->cpu_state.ebp = src->cpu_state.ebp;
    dest->cpu_state.esp = src->cpu_state.esp;

    dest->cpu_state.ebx = src->cpu_state.ebx;
    dest->cpu_state.edx = src->cpu_state.edx;
    dest->cpu_state.ecx = src->cpu_state.ecx;
    dest->cpu_state.eax = src->cpu_state.eax;

    dest->cpu_state.eip = src->cpu_state.eip;
    dest->cpu_state.cs = src->cpu_state.cs;
    dest->cpu_state.eflags = src->cpu_state.eflags;


    return 0;
}


vfs_file_t process_get_by_fd(process_t* p, uint32_t fd){
    return (vfs_file_t)(list_get_idx(p->file_descriptors, fd));
}

uint32_t process_add_open_file(process_t* p, vfs_file_t f){
    list_add(p->file_descriptors, f);
    p->fcount++;

    return p->fcount;
}